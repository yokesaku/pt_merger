CC      = g++
CFLAGS  = -std=c++11 -g -MMD -MP 
LDFLAGS =
LIBS    = 
INCLUDE = -I ./include
SRC_DIR = ./src
OBJ_DIR = ./obj
BIN_DIR = ./bin
SRC_SUF = .cxx
OBJ_SUF = .o
DIP_SUF = .d
BIN_SUF = 

TARGET  = pt_merger hoge

SOURCES = $(shell ls $(SRC_DIR)/*$(SRC_SUF)) 
EXT_SRC = $(filter-out $(addprefix $(SRC_DIR)/, $(addsuffix $(SRC_SUF),$(TARGET))), $(SOURCES))
OBJS    = $(subst $(SRC_DIR),$(OBJ_DIR), $(EXT_SRC:$(SRC_SUF)=$(OBJ_SUF)))
TARGETOBJ = $(addprefix $(OBJ_DIR)/, $(addsuffix $(OBJ_SUF), $(TARGET)))
TARGETBIN = $(addprefix $(BIN_DIR)/, $(TARGET))
DEPENDS = $(OBJS:$(OBJ_SUF)=$(DIP_SUF))

all: $(TARGETBIN)

$(TARGETBIN): $(OBJS) $(LIBS) $(TARGETOBJ)
	@if [ ! -d $(BIN_DIR) ]; \
		then echo "mkdir -p $(BIN_DIR)"; mkdir -p $(BIN_DIR); \
	fi
	echo $^
	$(CC) $(CFLAGS) -o $@ $(OBJ_DIR)/$(@F).o $(OBJS) $(LDFLAGS)

#$(OBJS): $(SOURCES)
$(OBJ_DIR)/%.o: $(SRC_DIR)/%$(SRC_SUF)
	@if [ ! -d $(OBJ_DIR) ]; \
		then echo "mkdir -p $(OBJ_DIR)"; mkdir -p $(OBJ_DIR); \
	fi
	$(CC) $(CFLAGS) $(INCLUDE) $(LDFLAGS) -o $@ -c $< 

clean:
	$(RM) -rf $(OBJ_DIR) $(BIN_DIR) $(DEPENDS)

-include $(DEPENDS)
