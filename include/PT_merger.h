#ifndef	PT_MERGER_H
#define	PT_MERGER_H

#include <string>

class PT_merger
{

	public:
		void Write_pt_merger(std::string &out_filename);
        void MakeTestLUT(const std::string &output);
        void MakeCoeFile(const std::string &output);
};

#endif
