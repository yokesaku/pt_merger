#include "PT_merger.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

void PT_merger::Write_pt_merger(std::string &out_filename){
	std::ofstream output_file(out_filename);
	if(output_file.fail()){
		std::cerr << "Error. Cannot open output file." << std::endl;
		return;
	}

	int pt_merger[(1<<12)] = {};

	for(int i=0;i<(1<<12);i++){
		int pt0 = (i >> 8) & 0xf; // [11:8] bits of i
		int pt1 = (i >> 4) & 0xf; // [7:4]  bits of i
		int pt2 =  i       & 0xf; // [3:0]  bits of i

		int max_pt = std::max(pt0, std::max(pt1, pt2));
		pt_merger[i] = max_pt;
	}

	int cnt = 0;
	for(int ssc=0;ssc<19;ssc++){
		for(int i=0;i<(1<<12); i+=4){
			int data0 = pt_merger[i    ];
			int data1 = pt_merger[i + 1];
			int data2 = pt_merger[i + 2];
			int data3 = pt_merger[i + 3];
	  			uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
			output_file << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
			cnt++;
		}
	}
	std::cout << cnt << std::endl;
	output_file.close();

	return;
}

void PT_merger::MakeTestLUT(const std::string &output){

    int pt_merger[(1<<12)] = {};
    
    int cnt = 0;
    int flg = 1;
    for(int i=0;i<(1<<12);i++){
        pt_merger[i] = cnt;
        cnt += flg;
        if(cnt == 0 || cnt == 15){
            flg *= -1;
        }
    }

	std::ofstream output_file(output);
	if(output_file.fail()){
		std::cerr << "Error. Cannot open output file." << std::endl;
		return;
	}

	cnt = 0;
	for(int ssc=0;ssc<19;ssc++){
		for(int i=0;i<(1<<12); i+=4){
			int data0 = pt_merger[i    ];
			int data1 = pt_merger[i + 1];
			int data2 = pt_merger[i + 2];
			int data3 = pt_merger[i + 3];
	  			uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
			output_file << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
			cnt++;
		}
	}
	std::cout << cnt << std::endl;
	output_file.close();
    return;
}

void PT_merger::MakeCoeFile(const std::string &output){

    const int LUT_depth = (1<<12);
    int pt_merger[(1<<12)] = {};

	for(int i=0;i<(1<<12);i++){
		int pt0 = (i >> 8) & 0xf; // [11:8] bits of i
		int pt1 = (i >> 4) & 0xf; // [7:4]  bits of i
		int pt2 =  i       & 0xf; // [3:0]  bits of i

		int max_pt = std::max(pt0, std::max(pt1, pt2));
		pt_merger[i] = max_pt;
	}

	std::ofstream ofs(output);
    ofs << "; .coe file for PT_MERGER LUT. " << std::endl;
    ofs << "; LUT depth = 4096, LUT width = 4bit" << std::endl;
    ofs << "memory_initialization_radix=16;" << std::endl;
    ofs << "memory_initialization_vector=" << std::endl;
    for(int addr=0;addr<LUT_depth;addr++){
        ofs << std::hex << pt_merger[addr];
        if(addr != LUT_depth-1) ofs << "," << std::endl;
        else                  ofs << ";" << std::endl;
    }

	ofs.close();
    
    return;
}
