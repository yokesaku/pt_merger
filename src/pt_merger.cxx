#include "PT_merger.h"
#include <iostream>
#include <string>

int main(int argc, char** argv){

	PT_merger ptm;
	std::string input_file = "pt_merger.txt";
	std::string input_file_test = "pt_merger_test.txt";
	ptm.Write_pt_merger(input_file);
	ptm.MakeTestLUT(input_file_test);
    ptm.MakeCoeFile("pt_merger.coe");
	
	return 0;
}
